import React ,{Component,Suspense} from 'react';
import { Container } from '../../node_modules/react-bootstrap';
import '../home/home.style.css';
import Loader from  '../common/loader.component';
const Meal = React.lazy(()=>import('../common/meal.component'));

class Home extends Component{
    constructor(){
        super();
        this.state = {
            meal:'',
            isloading:true
        }
    }
componentDidMount(){
    fetch('https://www.themealdb.com/api/json/v1/1/filter.php?a=Indian')
    .then(reponse=>reponse.json())
    .then((result)=>{
        this.setState({
            meal:result.meals,
            isloading:false
        })
    })
}
render(){
    
    if(this.state.isloading){
        return(
                <Container>
                <Loader/>
                </Container>
        )
    }else{
        return(
                <Container>
                    <div className="row mt-5 mr-5 ml-5">
                        <input className="form-control" placeholder="Search for a Meal"/>
                    </div>
                    <div className="center">
                    <h3>Indian Meals</h3>
                    <div className="row"> 
                    <Suspense fallback={<div><Loader/></div>}>           
                    <Meal>{this.state.meal}</Meal>
                    </Suspense>
                    </div>
                    </div>
                    

                </Container>
            
            
        )
    }
    
}
}

export default Home;