import React ,{Component }from 'react';
import { Container } from '../../node_modules/react-bootstrap';
import Loader from '../common/loader.component';
import Ingredients from '../recipeOverview/Ingredients.component';
import '../recipeOverview/overview.style.css';
class Overview extends Component {
constructor(){
    super();
    this.state = {
        overdata:'',
        food_id:this.props
    }
}
componentDidMount(){
fetch('https://www.themealdb.com/api/json/v1/1/lookup.php?i='+this.props.match.params.foodId)
.then(response=>response.json())
.then((result)=>{
    this.setState({
        overdata:result
    })
})
}
    render(){

        if(this.state.overdata===''){
            return( <div>
                <Container>
                <Loader/>
                </Container>
            </div>
        )
        }else{
            let food_detail = this.state.overdata.meals[0];
            return(
                <div>
                <Container>
                   <div className="row mt-5 foodDes">
                       <div className="col-md-4">
            <h4>{food_detail.strMeal}</h4>
                           <img className="img-thumbnail" alt={food_detail.strMeal} src={food_detail.strMealThumb}></img>
                       </div>
                       <div className="col-md-8">
                       <h4>Ingredients</h4>
                       <div className="row">
                       <Ingredients>{food_detail}</Ingredients>
                       </div>
            
                       </div>
    
                   </div>
                   <div className="row mt-5">
                   <div className="col-md-12 Instructions">
                       <h5>Instructions</h5>
            <p>{food_detail.strInstructions}</p>
                   </div>
                   </div>
                </Container>
            </div>
                    
            )
        }
           
    }
}


export default Overview;