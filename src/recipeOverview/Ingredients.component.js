import React ,{Component }from 'react';
import {Link} from 'react-router-dom';
class Ingredients extends Component {
    render(){
        let html = [];
       for(let i=1;i<=20;i++){
           if(this.props.children['strIngredient'+i]!==''){
            html.push({'name':this.props.children['strIngredient'+i],'strMeasure':this.props.children['strMeasure'+i]})
           }
        
       }
       console.log(html);
        return(
            html.map((Ingredients)=>{
                return(
                    <div className="col-md-2 Ingredients" key={Ingredients.name}>
            <img className="img-fluid Ingredients_Img" alt={Ingredients.name} src={'https://www.themealdb.com/images/ingredients/'+Ingredients.name+'.png'}></img>
                <Link to="/"><p>{Ingredients.strMeasure}</p></Link>
             </div>  
                )
            })
               
        )
                
}
}


export default Ingredients;