import React ,{Component }from 'react';

class Loader extends Component {
    render(){
       
        return(
            <div className="d-flex justify-content-center">
            <div className="spinner-grow spinner-grow-lg" role="status">
  <span className="sr-only">Loading...</span>
</div>
          </div>
                )        
        
    }
}


export default Loader;