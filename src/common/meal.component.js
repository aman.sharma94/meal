import React ,{Component }from 'react';
import { Card } from '../../node_modules/react-bootstrap';
import LazyLoad from 'react-lazyload';
import '../common/meal.styles.css';

class MealCard extends Component {
    render(){
        let meal = this.props.children;
        let newMeal = [];
        for(let i=0;i<8;i++){
            newMeal.push({"idMeal":meal[i].idMeal,"strMealThumb":meal[i].strMealThumb})
        }
        return(
            newMeal.map((data)=>{
                return( 
                    <div className="col-md-3 MealMaindiv" key={data.idMeal}>
                        <LazyLoad>
                <Card className="mt-3 MealDiv">
                <a href={"/recipie-overview/"+data.idMeal} >
                    <Card.Img variant="top" width="100" src={data.strMealThumb}></Card.Img>
                    </a>
                </Card>
                </LazyLoad>
                </div>
                )
            })
        )
               
                
                    
            
            
            
        
    }
}


export default MealCard;