import React from 'react';
import {Navbar} from 'react-bootstrap';

class Header extends React.Component{
    
   
render(){
   
    return(
     <Navbar bg="light" expand="lg">
     <Navbar.Brand  href="/">Meal Box</Navbar.Brand>
     <Navbar.Toggle aria-controls="basic-navbar-nav" />
     <Navbar.Collapse>
         <Nav className="mr-auto">
        <Nav.Link href="/">Home</Nav.Link>
        <Nav.Link href="/category">Category</Nav.Link>
         </Nav>
         <Form inline>
      <FormControl type="text" placeholder="Search" className="mr-sm-2" />
      <Button variant="outline-success" className="mr-2">Search</Button>
    </Form>
     </Navbar.Collapse>
     </Navbar>   
    )
}
}

export default Header;