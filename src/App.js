import React from 'react';
import Home from './home/home.component';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Route, BrowserRouter  as  Router  } from 'react-router-dom';
import Category from './category/category.component';
import Overview from './recipeOverview/overview.component';
import {Navbar} from 'react-bootstrap';
import logo from '../src/logo-small.png';
import '../src/App.css';
function App() {
  return (
    <div className="HeadDiv">
    <Navbar  expand="lg" className="NavBar">
     <Navbar.Brand  href="/"><img className="Logod"  src={logo} alt="logo"/></Navbar.Brand>
    
     </Navbar> 
      <Router> 
      <Route exact path="/"><Home/></Route>
      <Route path="/category"><Category/></Route>
      <Route path="/recipie-overview/:foodId" render={(props) => <Overview {...props} />}></Route>
  </Router>
  <footer className="mt-5 ">
    <div className="container">
      <div className="row">
        <div className="col-md-6 font-weight-bold">@2020 Recipe app<br></br>Built by Aman</div>
        <div className="col-md-6 font-weight-bold float-right">
         <p className="float-right"> Api Credit   <a href="https://www.themealdb.com/">The MealDb</a></p>
        </div>
      </div>
    </div>
  </footer>
  </div>
  );
}

export default App;
